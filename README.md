# Simple Contact CRUD

### Environment :
1. Source code base using react-native cli
2. UI using native-base
3. State management using redux
4. Middleware using redux-saga
5. Form management using redux-form
6. The unit test is not implemented yet
7. Backend API documentation : https://simple-contact-crud.herokuapp.com/documentation
8. Cloud file storage using OSS Aliyun

### Setup for android :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. npm install
3. react-native run-android

### Setup for ios :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. npm install
3. cd ios
4. pod install
5. react-native run-ios

### Generate non signed apk release 
1. On terminal or command prompt run command "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle"
2. cd android
3. ./gradlew assembleRelease (for linux/mac) or .\gradlew assembleRelease (for windows)
4. check apk file (bundle) in directory /android/app/build/outputs/apk

### Generate keystore for signed release aab / bundle
1. On terminal or command prompt run command "sudo keytool -genkey -v -keystore keystorename-key.keystore -alias keystorename-alias -keyalg RSA -keysize 2048 -validity 10000"
2. Copy file keystore -> android/app
3. Setup keystore in android/app/build.gradle 
4. On terminal or command prompt run command "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle"
5. cd android
6. ./gradlew bundleRelease (for linux/mac) or .\gradlew bundleRelease (for windows)
7. Or you can generate signed apk from Android Studio using new keystore
8. check aab file (bundle) in directory /android/app/build/outputs/bundle

# Lets try




