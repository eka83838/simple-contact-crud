import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {Root} from 'native-base';
import {Provider} from 'react-redux';
import {store} from './store/store';
import {AppStack} from './navigations/AppNavigator';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Root>
          <NavigationContainer>
            <AppStack />
          </NavigationContainer>
        </Root>
      </Provider>
    );
  }
}
