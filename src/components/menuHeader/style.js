import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
    Icon : {
        fontSize: 18,
        color:'#002f5f'
    },
    WifiIcon : {
        fontSize: 18,
        color:'green'
    },
    Text : {
        marginLeft:-3,
        color:'#002f5f'
    },
    Header : {
        backgroundColor:'white',
        borderBottomWidth:0,
    },
    title : {
        fontSize:14,
        fontWeight:'bold',
        color:'#002f5f'
    },
    textBack : {
        color:'black', 
        fontSize:14,
        marginTop:Platform.OS == 'android' ? 0 : -15,
        fontWeight:'bold',
        color:'#002f5f'
    }
})