import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import styles from './style';

export default class ContactCardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  cardHeader = (data) => (
    <View style={styles.CardHeader}>
      <View style={styles.HeaderOrderNo}>
        <Text style={styles.TextOrderNo}>
          {data.firstName + ' ' + data.lastName}
        </Text>
      </View>
    </View>
  );

  btnControl = () => {
    const {handleDelete, handleDetail} = this.props;

    return (
      <View style={styles.MiddleBodyContent}>
        <View style={styles.wrapperBtnControl}>
          <TouchableOpacity style={styles.BtnBlue} onPress={handleDetail}>
            <Text style={styles.BtnText}>Edit</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.BtnRed} onPress={handleDelete}>
            <Text style={styles.BtnText}>Hapus</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  contentImage = (data) => (
    <View style={styles.FullnameContainer}>
      {data.photo !== 'N/A' && (
        <Image source={{uri: data.photo}} style={styles.imageSize} />
      )}
    </View>
  );

  contentBodyText = (data) => (
    <View style={styles.IdNoContainer}>
      <Text style={styles.TextFullname}>{'Age : ' + data.age}</Text>
    </View>
  );

  cardBody = (data) => (
    <View style={styles.CardBody}>
      <View style={styles.TopBodyContent}>
        {this.contentImage(data)}
        {this.contentBodyText(data)}
      </View>
      {this.btnControl()}
    </View>
  );

  render() {
    const {data} = this.props;

    return (
      <TouchableOpacity style={styles.Content}>
        {this.cardHeader(data)}
        {this.cardBody(data)}
      </TouchableOpacity>
    );
  }
}
