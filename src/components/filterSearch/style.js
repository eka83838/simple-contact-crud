import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    SearchInput : {
        borderRadius:5,
        borderColor:"#bbb",
        borderWidth:1,
        paddingLeft:10,
        width:"100%",
        height:40,
    },
})