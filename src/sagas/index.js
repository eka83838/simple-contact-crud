import {all, fork} from "redux-saga/effects";
import {contactListWatch, contactDetailWatch, contactDeleteWatch, contactAddWatch, contactUpdateWatch} from "./contact"

export default function* sagas() {
  yield all([
    fork(contactListWatch),
    fork(contactDetailWatch),
    fork(contactDeleteWatch),
    fork(contactAddWatch),
    fork(contactUpdateWatch)
  ]);
}
