
export const FormContactDetail = (values) => {
    const errors = {}

    if (!values.firstName || values.firstName === "") {
      errors.firstName = 'Nama depan harus di isi'
    }

    if (!values.lastName || values.lastName === "") {
      errors.lastName = 'Nama belakang harus di isi'
    }

    if (!values.age || values.age == "") {
      errors.age = 'Usia harus di isi'
    }

    return errors
  }
  