export async function filterFetch(url, options) {
    return await fetch(url, options).then(res => {
        if (res.status !== 200 && res.status !== 201 && res.status !== 202) { 
            throw new Error('Proses gagal. Status code : ' + res.status) 
        }
        return res.json()
    }).then(json => {
        return json
    })
}