import React, { Component } from 'react';
import { View } from 'react-native';
import { Spinner, Container, Text } from 'native-base';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 'Auth',
      spinner: true
    };
  }

  componentDidMount = async () => {
    this.setState({spinner: true})
    setTimeout(() => this.setState({spinner: false}),
      2000,
    );
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const {spinner} = this.state
    
    if (spinner == false && prevState.spinner !== spinner) {
      this.props.navigation.navigate('ContactList')
    }
  }

  render() {
    return (
      <Container style={{flex:1, alignItems:'center', justifyContent:'center'}}>
        <View style={{width:'100%'}}>
          <Text 
            style={{fontSize:30, fontWeight:'bold', alignSelf:'center'}}
          >CONTACT CRUD</Text>
        </View>
        <View style={{position:'absolute', width:'100%', bottom:0, marginBottom:'20%'}}>
          <Spinner
            visible={this.state.spinner}
            color='black'
          />
          <Text style={{fontSize:14, alignSelf:'center'}}>Loading...</Text>
        </View>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ 

  }, dispatch);
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(Auth);
