import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    ScrollView : {
        width:'100%',
        height:"90%",
        paddingTop:10,
        paddingBottom:100,
    },
    ScrollViewBottom : {
        width:'100%',
        height:100
    },
    ButtonContainer : { 
        width: '100%',
        alignItems: 'center',
        backgroundColor:'transparent',
        marginBottom: 10
    },
    SearchInput : {
        borderRadius:5,
        borderColor:"#bbb",
        borderWidth:1,
        paddingLeft:10,
        width:"100%",
        height:40,
    },
    ButtonFilterSubmit : { 
        marginVertical:20,
        alignItems: 'center', 
        justifyContent: 'center', 
        height: 40, 
        width: "90%", 
        borderRadius: 5, 
        backgroundColor: '#5BC0DE',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3
    },
    Button : {
        width:'25%',
        top:15,
        height:40,
        position:'absolute',
        right:12,
        borderRadius:5,
        backgroundColor:'#5BC0DE',
        justifyContent:'center',
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 5
    },
    ButtonTitle : {
        fontSize:18,
        color:'white'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    CancelIconAndroid : {
        fontSize:25,
        color:'#002f5f',
        marginBottom:10,
        marginTop:10,
        left:10
    },
    CancelIconIos : {
        fontSize:25,
        color:'#002f5f',
        marginBottom:10,
        marginTop:50,
        left:10
    },
    FormContainer: { 
        height: '100%', 
        marginTop:10, 
        alignItems:'center',
        flex:1
    },
    FilterButtonTitle : { 
        fontSize: 18, 
        color: 'white'
    },
    ResubmitSpinner: {
        alignSelf:'center', 
        justifyContent:'center', 
        zIndex:3000
    },
    SDContainerStyle : { 
        padding: 5,
        width:'93%',
    },
    SDItemStyle : {
        padding: 12,
        height:42,
        marginTop: 2,
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5
    },
    SDItemTextStyle : {  
    },
    SDItemsContainerStyle : { 
        maxHeight:  135
    },
    ErrorDesc : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        fontSize:10, 
    },
    ErrorDescDropdown : { 
        color:'red',
        alignSelf:'stretch',
        textAlign:'right',
        right:33,
        fontSize:10
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        height: '100%',
        zIndex:2000,
        left: 0,
        top: 0,
        opacity: 0.3,
        alignContent:'center',
        justifyContent:'center',
        backgroundColor: 'black',
        width: "100%"
    },
    viewHeader : {
        height:60, 
        backgroundColor:'white', 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 3,
        paddingVertical: 10,
        paddingHorizontal: 10
    }
    
})