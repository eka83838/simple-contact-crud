import React, {Component} from 'react';
import {reduxForm, Field, change, reset} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {launchImageLibrary} from 'react-native-image-picker';
// import {uploadOssFile} from '../../../utils/uploadImage';
import MenuHeader from "../../../components/menuHeader";
import {TouchableOpacity, Text, Alert, View, Image, BackHandler, Platform, ScrollView} from 'react-native';
import {Container, Label, Input, Item, Row, Form} from 'native-base';
import {contactDetail, contactAdd, contactUpdate, contactList} from "../../../actions/contact";
import Spinner from 'react-native-loading-spinner-overlay';
import {styles} from './style';
import {FormContactDetail} from "../../../utils/validates/FormContactDetail"

export const renderField = ({
  input,
  type,
  label,
  placeholder,
  editable,
  keyboardType,
  maxLength,
  placeholderTextColor,
  secureTextEntry,
  meta: {touched, error, warning},
}) => {
  var hasError = false;

  if (touched && error !== undefined) {
    hasError = true;
  }

  return (
    <View style={styles.input}>
      <Label style={styles.label}>{label}</Label>
      <Item
        fixedLabel
        style={{
          borderColor: hasError ? 'red' : '#666',
          height: 40,
        }}>
        <Input
          {...input}
          type={type}
          editable={editable}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          placeholderTextColor={placeholderTextColor ? 'black' : '#A6AAB4'}
          secureTextEntry={secureTextEntry}
          style={{
            fontSize: 14,
            marginLeft: -5,
            color: editable ? 'black' : '#A6AAB4',
          }}
        />
      </Item>
      {hasError ? <Text style={styles.errorDesc}>{error}</Text> : null}
    </View>
  );
};

class ContactDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: this.props.route.params ? 'Detail Kontak' : 'Tambah Kontak',
      prevUri: 'https://www.tenforums.com/geek/gars/images/2/types/thumb__ser.png',
      uri: 'https://www.tenforums.com/geek/gars/images/2/types/thumb__ser.png',
      spinner: false
    };
  }

  componentDidMount = async () => {
    const {contactDetail} = this.props;
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    if (this.props.route.params) {
      contactDetail(this.props.route.params.id);
      this.setState({spinner: true});
    }
  };

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
      this.props.navigation.goBack();
      return true;
    }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    const {contactDetailResult, contactDetailError, contactAddError, contactAddResult, contactUpdateError, contactUpdateResult, contactList} = this.props;

    if (contactDetailError && prevProps.contactDetailError !== contactDetailError) {
      Alert.alert('Gagal', contactDetailError.message);
    }

    if (contactDetailResult && prevProps.contactDetailResult !== contactDetailResult) {
      this.setState({ spinner: false });
      this.props.updateField('formContactDetail', 'firstName', contactDetailResult.firstName);
      this.props.updateField('formContactDetail', 'lastName', contactDetailResult.lastName);
      this.props.updateField('formContactDetail', 'age', contactDetailResult.age.toString());
      if (contactDetailResult.photo !== "N/A") {
        this.setState({ uri: contactDetailResult.photo});
      }
    }

    if (contactAddError && prevProps.contactAddError !== contactAddError) {
      this.setState({spinner: false});
      Alert.alert('Gagal', contactAddError.message);
    }

    if (contactAddResult && prevProps.contactAddResult !== contactAddResult) {
      this.setState({spinner: false});
      Alert.alert('Berhasil', contactAddResult.message);
      contactList();
      this.props.navigation.navigate('ContactList');
    }

    if (contactUpdateError && prevProps.contactUpdateError !== contactUpdateError) {
      this.setState({spinner: false});
      Alert.alert('Gagal', contactUpdateError.message);
    }

    if (contactUpdateResult && prevProps.contactUpdateResult !== contactUpdateResult) {
      this.setState({spinner: false});
      Alert.alert('Berhasil', contactUpdateResult.message);
      contactList();
      this.props.navigation.navigate('ContactList');
    }
  };

  goBack = async () => {
    this.props.navigation.goBack();
    this.props.resetForm('formContactDetail');
  };

  changeImageHandle = async () => {
    const options = {
      mediaType: 'photo',
      maxWidth: 2000,
      maxHeight: 2000,
    };
    launchImageLibrary(options)
      .then((response) => {
        if (response.didCancel) {
          return;
        }
        this.setState({uri: response.assets[0].uri});
      })
      .catch((err) => {
        Alert.alert('Upload Failed!', err.message);
      });
  };

  ContactDetailSubmit = async (data) => {
    const {uri, prevUri} = this.state;
    const {contactAdd, contactUpdate, contactDetailResult} = this.props;

    this.setState({spinner: true});
    if (uri !== prevUri) {
      try {
        // const usrImg = await uploadOssFile(uri);
        this.setState({prevUri: usrImg, uri: usrImg});
        if (this.props.route.params) {
          contactUpdate({
            firstName: data.firstName,
            lastName: data.lastName,
            age: parseInt(data.age),
            photo: uri
          }, contactDetailResult.id);
        } else {
          contactAdd({
            firstName: data.firstName,
            lastName: data.lastName,
            age: parseInt(data.age),
            photo: uri
          });
        }
      } catch (error) {
        Alert.alert('Gagal', error.message);
      }
    } else {
      if (this.props.route.params) {
        contactUpdate({
          firstName: data.firstName,
          lastName: data.lastName,
          age: parseInt(data.age),
          photo: contactDetailResult.photo
        }, contactDetailResult.id);
      } else {
        contactAdd({
          firstName: data.firstName,
          lastName: data.lastName,
          age: parseInt(data.age),
          photo: 'N/A'
        });
      }
    }
  };

  renderImageForm = () => (
    <Row style={styles.row}>
      <Image
        style={styles.image}
        source={{
          uri: this.state.uri,
          width: 80,
          height: 80,
        }}
      />
      <TouchableOpacity
        onPress={this.changeImageHandle}
        style={styles.editImgTouchable}>
        <Image
          style={styles.editImg}
          source={{
            uri: 'https://www.tenforums.com/geek/gars/images/2/types/thumb__ser.png',
            width: 80,
            height: 80,
          }}
        />
      </TouchableOpacity>
    </Row>
  );

  generateField = (props) => {
    const keyBoard = props.keyboardType && {keyboardType: props.keyboardType};
    const maxLength = props.maxLength && {maxLength: props.maxLength};

    return (
      <Field
        name={props.name}
        type={props.type}
        component={renderField}
        editable={props.editable}
        label={props.label}
        {...keyBoard}
        {...maxLength}
      />
    );
  };

  renderForm = () => {
    const {handleSubmit, submitting} = this.props;

    return (
      <Form style={{paddingHorizontal: '12%', paddingVertical: '10%'}}>
        {this.generateField({
          name: 'firstName',
          type: 'text',
          editable: true,
          label: 'Nama Depan',
        })}
        {this.generateField({
          name: 'lastName',
          type: 'text',
          editable: true,
          label: 'Nama Belakang',
        })}
        {this.generateField({
          name: 'age',
          type: 'text',
          editable: true,
          label: 'Usia',
          maxLength: 3,
          keyboardType: 'number-pad'
        })}
        <TouchableOpacity
          style={styles.btnKirim}
          onPress={handleSubmit(this.ContactDetailSubmit)}
          disabled={submitting}
        >
          <Text style={styles.text}>Kirim</Text>
        </TouchableOpacity>
      </Form>
    );
  };

  render() {
    return (
      <Container>
        <MenuHeader title={this.state.screen} goBack={this.goBack} />
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <ScrollView
          disableKBDismissScroll={false}
          style={
            Platform.OS !== 'android' && this.state.showKeyboard == true
              ? {marginBottom: 250}
              : null
          }
        >
          {this.renderImageForm()}
          {this.renderForm()}
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    contactDetailResult: state.contactDetail.result,
    contactDetailLoading: state.contactDetail.loading,
    contactDetailError: state.contactDetail.error,
    contactAddResult: state.contactAdd.result,
    contactAddLoading: state.contactAdd.loading,
    contactAddError: state.contactAdd.error,
    contactUpdateResult: state.contactUpdate.result,
    contactUpdateLoading: state.contactUpdate.loading,
    contactUpdateError: state.contactUpdate.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      contactList,
      contactDetail,
      contactAdd,
      contactUpdate,
      updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
      resetForm: (form) => dispatch(reset(form)),
    }, dispatch);
}

ContactDetail = reduxForm({
  form: 'formContactDetail',
  enableReinitialize: true,
  validate: FormContactDetail
})(ContactDetail);

export default connect(mapStateToProps, matchDispatchToProps)(ContactDetail);
