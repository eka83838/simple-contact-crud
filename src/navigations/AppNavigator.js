import * as React from 'react';
import { createStackNavigator,TransitionSpecs,HeaderStyleInterpolators   } from '@react-navigation/stack';
import Auth from '../screens/auth';
import ContactList from '../screens/contact/list'
import ContactDetail from '../screens/contact/detail'

const Stack = createStackNavigator();

const MyTransition = {
    gestureDirection: 'horizontal',
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
    headerStyleInterpolator: HeaderStyleInterpolators.forFade,
  }

export function AppStack() {
    return (
        <Stack.Navigator
            headerMode = "none"
            initialRouteName= "Auth" 
        >
            <Stack.Screen name='Auth' component={Auth} />
            <Stack.Screen name='ContactList' component={ContactList} />
            <Stack.Screen name='ContactDetail' component={ContactDetail} />
        </Stack.Navigator> 
    );
}