export const CONTACT_LIST_PROCESS = "CONTACT_LIST_PROCESS";
export const CONTACT_LIST_SUCCESS = "CONTACT_LIST_SUCCESS";
export const CONTACT_LIST_ERROR = "CONTACT_LIST_ERROR";

export const CONTACT_DETAIL_PROCESS = "CONTACT_DETAIL_PROCESS";
export const CONTACT_DETAIL_SUCCESS = "CONTACT_DETAIL_SUCCESS";
export const CONTACT_DETAIL_ERROR = "CONTACT_DETAIL_ERROR";

export const CONTACT_ADD_PROCESS = "CONTACT_ADD_PROCESS";
export const CONTACT_ADD_SUCCESS = "CONTACT_ADD_SUCCESS";
export const CONTACT_ADD_ERROR = "CONTACT_ADD_ERROR";

export const CONTACT_DELETE_PROCESS = "CONTACT_DELETE_PROCESS";
export const CONTACT_DELETE_SUCCESS = "CONTACT_DELETE_SUCCESS";
export const CONTACT_DELETE_ERROR = "CONTACT_DELETE_ERROR";

export const CONTACT_UPDATE_PROCESS = "CONTACT_UPDATE_PROCESS";
export const CONTACT_UPDATE_SUCCESS = "CONTACT_UPDATE_SUCCESS";
export const CONTACT_UPDATE_ERROR = "CONTACT_UPDATE_ERROR";

export function contactList() {
    return {
        type: CONTACT_LIST_PROCESS
    };
}

export function contactDetail(data) {
    return {
        type: CONTACT_DETAIL_PROCESS,
        data: data
    };
}

export function contactAdd(data) {
    return {
        type: CONTACT_ADD_PROCESS,
        data: data
    };
}

export function contactDelete(data) {
    return {
        type: CONTACT_DELETE_PROCESS,
        data: data
    };
}

export function contactUpdate(data, param) {
    return {
        type: CONTACT_UPDATE_PROCESS,
        data: data,
        param: param
    };
}